fs=require('fs');

var sounds = {
	root: __dirname+"/audio/"
};
var files = fs.readdirSync(__dirname+"/audio");
files.forEach(function processFile(file) {
	if( file.match(/mp3$/) ) {
		sounds[file.match(/(.+)\.mp3/)[1]]=sounds.root+file;
	}
});

module.exports=sounds;